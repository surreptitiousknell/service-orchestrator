package eu.proszowski.surreptitiousknell.serviceorchestrator.wordsclassifier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import java.util.Set;
import java.util.UUID;

public class WordsClassifierClient {

    @Value("${wordsClassifier.url}")
    private String BASE_URL;

    @Autowired
    private RestTemplate restTemplate;

    public void getAllWordsForUserId(final UUID userId){
        final ResponseEntity<Set> wordDtos = restTemplate.getForEntity(String.format("%s/getAllWordsForUserId/%s", BASE_URL, userId), Set.class);
    }

    public void classifyWords(final UUID userId, final Set<UUID> wordIds){
        final ResponseEntity<Set> wordDtos = restTemplate.postForEntity(String.format("%s/classifyWordsForUserId/%s", BASE_URL, userId), wordIds, Set.class);
    }

    public void markWords(final UUID userId, final Set<UUID> wordIds){
        restTemplate.postForEntity(String.format("%s/markWordsForUserId/%s", BASE_URL, userId), wordIds, Set.class);
    }

}
