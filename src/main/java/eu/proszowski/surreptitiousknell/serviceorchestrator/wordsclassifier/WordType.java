package eu.proszowski.surreptitiousknell.serviceorchestrator.wordsclassifier;

public enum WordType {
    KNOWN,
    UNKNOWN,
    LEARNING_IN_PROGRESS
}
