package eu.proszowski.surreptitiousknell.serviceorchestrator.wordsclassifier;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
class WordDto {

    @NonNull
    private UUID wordId;
    @NonNull
    private WordType wordType;
}
