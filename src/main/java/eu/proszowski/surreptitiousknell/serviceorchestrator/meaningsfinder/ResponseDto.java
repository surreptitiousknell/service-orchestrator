package eu.proszowski.surreptitiousknell.serviceorchestrator.meaningsfinder;

import lombok.Builder;
import lombok.Data;
import java.util.List;

@Data
class ResponseDto {
    List<WordWithMeaningsDto> wordsWithMeanings;
    List<String> unrecognizedWords;
}
