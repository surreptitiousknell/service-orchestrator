package eu.proszowski.surreptitiousknell.serviceorchestrator.meaningsfinder;

import lombok.Builder;
import lombok.Data;

@Data
public class WordDto {
    String id;
    String value;
}
