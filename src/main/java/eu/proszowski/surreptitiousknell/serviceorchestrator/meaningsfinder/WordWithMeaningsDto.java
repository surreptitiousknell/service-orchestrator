package eu.proszowski.surreptitiousknell.serviceorchestrator.meaningsfinder;

import lombok.Builder;
import lombok.Data;
import java.util.List;

@Data
public class WordWithMeaningsDto {
    private String id;
    private String value;
    private List<String> meanings;
}
