package eu.proszowski.surreptitiousknell.serviceorchestrator.meaningsfinder;

import lombok.Builder;
import lombok.Data;

@Data
class MeaningDto {
    private String value;
}
