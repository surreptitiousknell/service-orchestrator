package eu.proszowski.surreptitiousknell.serviceorchestrator.meaningsfinder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import java.util.Collection;

@Component
public class MeaningsFinderClient {

    @Value("${meaningsFinder.url}")
    private String BASE_URL;

    @Autowired
    private RestTemplate restTemplate;

    void findMeaningsForWords(final Collection<String> words){
        final ResponseEntity<ResponseDto> responseDto = restTemplate.postForEntity(BASE_URL + "/findMeaningsForWords", words, ResponseDto.class);
    }

}
