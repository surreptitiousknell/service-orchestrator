package eu.proszowski.surreptitiousknell.serviceorchestrator;

import lombok.Data;
import lombok.NonNull;
import org.junit.platform.commons.util.Preconditions;
import java.util.Arrays;
import java.util.List;

@Data
class WordString {
    static final List<Character> FORGIVEN_SIGNS = Arrays.asList('.', ',', '"', '\'', '\\', '/', '`', '*', '!', '@', '#', '$', '%', '^', '&', '(', ')', '>', '<', '-', ';', ':', '+', '=', '\n');

    @NonNull
    private String word;

    WordString(final String word) {
        FORGIVEN_SIGNS.forEach(
                sign -> Preconditions.condition(!word.contains(sign.toString()), "Word cannot contain sign '" + sign + "' but it contains")
        );
        this.word = word;
    }
}
