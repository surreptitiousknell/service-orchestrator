package eu.proszowski.surreptitiousknell.serviceorchestrator;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

class TextToWordsConverter {

    List<WordString> convert(final String text){
        return Arrays.stream(text.split(" "))
                .map(this::replace)
                .map(String::toLowerCase)
                .filter(s -> !s.isEmpty())
                .filter(s -> !s.isBlank())
                .map(WordString::new)
                .collect(Collectors.toList());
    }

    private String replace(String string){
        final List<Character> charactersToReplace = WordString.FORGIVEN_SIGNS;
        for (final Character character : charactersToReplace) {
            string = string.replace(character.toString(), "");
        }
        return string;
    }
}
